Для развертывания и тестирования API:

Установите необходимые зависимости, выполнив команду pip install -r req.txt
Создайте новый проект Django с помощью команды django-admin startproject myproject
Внесите изменения в файлы settings.py и urls.py вашего проекта, добавив вышеуказанный код
Создайте миграции для моделей с помощью команды python manage.py makemigrations и примените их с помощью команды python manage.py migrate
Запустите сервер API с помощью команды python manage.py runserver
Документация API доступна по адресу
    -http://localhost:8000/swagger/
    -http://localhost:8000/redoc/
    -http://localhost:8000/

Используйте инструменты, такие как cURL или Postman, для отправки запросов к конечным точкам API (например, http://localhost:8000/clients, http://localhost:8000/mailings и т. д.)




Все задачи, отмеченные красным в файле TZ.doc, выполнены. У меня не было времени физически выполнить оставшиеся задачи. https://t.me/Ravshan_Ismoilov