
FROM python:3.10.5

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /config

COPY Pipfile Pipfile.lock /config/
RUN pip install pipenv && pipenv install --system

COPY . /config/
