from django.contrib import admin

from mainapp.models import Client, Mailing, Message

class ClientAdmin(admin.ModelAdmin):
    list_display = ('id', 'phone_number', 'operator_code', 'tag', 'timezone')

class MailingAdmin(admin.ModelAdmin):
    list_display = ('id', 'start_time', 'end_time', 'message', 'filter_operator_code', 'filter_tag')

class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'status', 'mailing', 'client')

admin.site.register(Client, ClientAdmin)
admin.site.register(Mailing, MailingAdmin)
admin.site.register(Message, MessageAdmin)