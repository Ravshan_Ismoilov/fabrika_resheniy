from django.utils import timezone
from mainapp.models import Client, Mailing, Message
from requests import post, exceptions
# from celery import shared_task

def send_message_to_external_api(message):
    # Логика отправки сообщения на внешний API
    try:
        url = 'https://probe.fbrq.cloud/v1/send/{message.id}'
        headers = {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTg5NzM4NzEsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9SYXZzaGFuX0lzbW9pbG92In0.o-r3V8KhEIHs_wnXtwNSS_8X_mXPvp_3sivvTJFU3sc'
        }
        data = {
            'text': message.mailing.message,
            'phone': message.client.phone_number
        }
        # Отправка сообщения на внешний API
        response = post(url, headers=headers, data=data)
        if response.status_code == 200:
            message.status = 'sent'
        else:
            message.status = 'failed'
    except exceptions.RequestException:
        message.status = 'failed'

    message.save()


# Алгоритм обработки активных рассылок и отправки сообщений клиентам
def exists_client_mailing(client, mailing):
    return Message.objects.filter(client=client, mailing=mailing, status='sent').exists()

def send_mailings(mailing):
    current_time = timezone.now()
    if mailing.start_time <= current_time <= mailing.end_time:
        clients = Client.objects.filter(
                            operator_code=mailing.filter_operator_code,
                            tag=mailing.filter_tag)

        for client in clients:
            if not exists_client_mailing(client, mailing):
                message = Message.objects.create(
                        status='pending',
                        mailing=mailing,
                        client=client)
                # Отправка сообщения на внешний API
                send_message_to_external_api(message)
    
# @shared_task
def process_mailings():
    current_time = timezone.now()
    active_mailings = Mailing.objects.filter(
                                start_time__lte=current_time,
                                end_time__gte=current_time)

    for mailing in active_mailings:
        clients = Client.objects.filter(
                            operator_code=mailing.filter_operator_code,
                            tag=mailing.filter_tag)

        for client in clients:
            if not exists_client_mailing(client, mailing):
                message = Message.objects.create(
                                status='pending',
                                mailing=mailing,
                                client=client)
                # Отправка сообщения на внешний API
                send_message_to_external_api(message)