from django.urls import path, include

from rest_framework import routers
from mainapp.views import ClientViewSet, MailingViewSet, MessageViewSet
from mainapp.utils import process_mailings

router = routers.DefaultRouter()
router.register('clients', ClientViewSet)
router.register('mailings', MailingViewSet)
router.register('messages', MessageViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
