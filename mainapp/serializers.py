from rest_framework import serializers
from mainapp.models import Client, Mailing, Message


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'phone_number', 'operator_code', 'tag', 'timezone']

class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = [
            'id',
            'start_time', 'end_time',
            'message',
            'filter_operator_code', 'filter_tag',
            'get_total_sent_count'
        ]

class MessageSerializer(serializers.ModelSerializer):
    mailing = MailingSerializer(read_only=True)
    client = ClientSerializer(read_only=True)
    class Meta:
        model = Message
        fields = ['id', 'created_at', 'status', 'mailing', 'client']