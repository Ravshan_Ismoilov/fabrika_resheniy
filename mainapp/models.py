from django.db import models
from django.core.validators import RegexValidator


phoneNumber = RegexValidator(regex = r"^7\d{10}$")

class Client(models.Model):
    phone_number = models.CharField(validators = [phoneNumber], max_length = 11, unique = True, verbose_name="Номер телефона")
    operator_code = models.CharField(max_length=3, verbose_name="Код мобильного оператора")
    tag = models.CharField(max_length=100, verbose_name="Тег")
    timezone = models.CharField(max_length=100, verbose_name="Часовой пояс")

    def __str__(self):
        return f'{self.id}'
    
class Mailing(models.Model):
    start_time = models.DateTimeField(verbose_name="Дата и время запуска рассылки")
    end_time = models.DateTimeField(verbose_name="Дата и время окончании рассылки")
    message = models.TextField(verbose_name="Текст сообщения для доставки клиенту")
    filter_operator_code = models.CharField(max_length=3, verbose_name="Код мобильного оператора")
    filter_tag = models.CharField(max_length=100, verbose_name="Тег")

    def __str__(self):
        return f'{self.id}'
    
    def get_total_sent_count(self):
        return self.message_mailing.count()

class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата и время создания (отправки)")
    status = models.CharField(max_length=100, verbose_name="Статус отправки")
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name="message_mailing", verbose_name="ID рассылки")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="message_client", verbose_name="ID клиента")

    def __str__(self):
        return f'{self.id}'