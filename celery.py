import os
from celery import Celery
from datetime import timedelta
from mainapp.utils import process_mailings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')

app = Celery('config')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()



app.conf.beat_schedule = {
    'mailings-every-day': {
        'task': 'mainapp.utils.process_mailings',
        'schedule': timedelta(days=1),
    },
}